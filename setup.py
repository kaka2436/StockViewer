__author__ = 'Yueyi Zhang'

# Usage
#
# Use
# python3 setup.py build
# to generate all the files  (see the folder: build)
#
# Use
# python3 setup.py bdist_msi(or bdist_rpm, bdist_mac, bdist_dmg)
# to generate a distribution file (see the folder: dist)

import sys
from cx_Freeze import setup, Executable

# Under win32 platform, you should add libEGL.dll separately, do not use absolute path, use relative path
# libEGL.dll is a PyQt5 related file which could not be detected automatically by cx_Freeze. Users should add
# this file manually to guarantee the integrity.
includefiles = ['libEGL.dll']

base = None
if sys.platform == 'win32':
    base = 'Win32GUI'

options = {
     'build_exe': {
         'include_files': includefiles
     }
}

executables = [
    Executable('stock_viewer.py', base = base)
]

setup(name='StockViewer',
      version='0.1',
      description='Stock Viewer',
      options=options,
      executables=executables
      )

